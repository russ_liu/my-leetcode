# My Leetcode

This is my Leetcode practice.

## Using Vscode to sign in Leetcode

1. Open browser with develop mode.
2. Login leetcode.com on browser.
3. Find **problemset/all/** request via develop mode & copy header's cookie.
![](find_cookie.png)
4. **Ctrl + Shrift + P** on vscode & type: **LeetCode: Sign in**
5. Typing in username or email then enter.
6. Paste cookie then enter.