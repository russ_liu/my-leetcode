package questions

/*
 * @lc app=leetcode id=3 lang=golang
 *
 * [3] Longest Substring Without Repeating Characters
 */

// @lc code=start
// 1. 每個迴圈用 {結束位置 - 開始位置} 計算長度，並跟上次的長度選擇比較大的
// 2. 紀錄每種字的最後的位置
// 3. 當發現開始重複字時，會根據這個字上次位置 vs. 目前的開始位置，選擇比較大的當作新的開始位置
func lengthOfLongestSubstring(s string) int {
	var maxLen, start int
	charMap := make(map[rune]int)
	for idx, char := range s {
		end := idx + 1
		if _, ok := charMap[char]; ok {
			start = max(start, charMap[char])
		}

		maxLen = max(maxLen, end-start)
		charMap[char] = end
	}

	return maxLen
}

func max(x, y int) int {
	if x > y {
		return x
	} else if y > x {
		return y
	} else {
		return x
	}
}

// @lc code=end
