package questions

/*
 * @lc app=leetcode id=17 lang=golang
 *
 * [17] Letter Combinations of a Phone Number
 */

// 利用 recursive loop讓每個字去找下一個按鍵的字並且把它們組起來，
// 直到遍歷全部的輸入按鍵。
// @lc code=start
func letterCombinations(digits string) []string {
	rtn := []string{}
	if len(digits) == 0 {
		return rtn
	}

	recursiveCombine(digits, &rtn, "")
	return rtn
}

func recursiveCombine(digits string, result *[]string, curStr string) {
	if len(digits) == 0 {
		*result = append(*result, curStr)
		return
	}
	num := digits[0]
	for _, s := range keys[num] {
		recursiveCombine(digits[1:], result, curStr+string(s))
	}
}

var keys = map[byte]string{
	'0': "",
	'1': "",
	'2': "abc",
	'3': "def",
	'4': "ghi",
	'5': "jkl",
	'6': "mno",
	'7': "pqrs",
	'8': "tuv",
	'9': "wxyz",
}

// @lc code=end
