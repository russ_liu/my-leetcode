package questions

import "sort"

/*
 * @lc app=leetcode id=4 lang=golang
 *
 * [4] Median of Two Sorted Arrays
 */

// @lc code=start
func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
	tmpSlice := append(nums1, nums2...)
	sort.Ints(tmpSlice)
	midIdx := len(tmpSlice) / 2
	if len(tmpSlice)%2 == 0 {
		return (float64(tmpSlice[midIdx-1] + tmpSlice[midIdx])) / 2
	} else {
		return float64(tmpSlice[midIdx])
	}
}

// @lc code=end
