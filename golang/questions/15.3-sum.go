package questions

import "sort"

/*
 * @lc app=leetcode id=15 lang=golang
 *
 * [15] 3Sum
 */

// @lc code=start
// https://www.youtube.com/watch?v=jzZsG8n2R9A&t=516s&ab_channel=NeetCode
// needCode 的解法：O(n^3) 的複雜度
func threeSum(nums []int) [][]int {
	var res [][]int

	if len(nums) == 0 {
		return res
	}

	sort.Ints(nums)
	for ix := 0; ix < len(nums)-2; ix++ {
		if ix > 0 && nums[ix] == nums[ix-1] {
			continue
		}
		left, right := ix+1, len(nums)-1
		for left < right {
			sum := nums[ix] + nums[left] + nums[right]

			if sum == 0 {
				res = append(res, []int{nums[ix], nums[left], nums[right]})
				left, right = left+1, right-1

				for left < right && nums[left] == nums[left-1] {
					left++
				}

				for left < right && nums[right] == nums[right+1] {
					right--
				}
			} else if sum > 0 {
				right--
			} else {
				left++
			}
		}
	}
	return res
}

// @lc code=end

// func threeSum(nums []int) [][]int {
// 	var res [][]int
// 	checkSet := make(map[int]interface{})
// 	sort.Ints(nums)

// 	for ix := 0; ix < len(nums); ix++ {

// 		firstNum := nums[ix]
// 		target := -firstNum
// 		if _, ok := checkSet[firstNum]; ok {
// 			continue
// 		}
// 		checkSet[firstNum] = nil

// 		tmpMap := make(map[int]int)
// 		checkSetInner := make(map[int]interface{})
// 		for jx := ix + 1; jx < len(nums); jx++ {
// 			value := nums[jx]

// 			_, ok := tmpMap[value]
// 			_, ok2 := checkSetInner[value]
// 			if ok && !ok2 {
// 				res = append(res, []int{firstNum, tmpMap[value], value})
// 				checkSetInner[value] = nil
// 				checkSetInner[target-value] = nil
// 			} else {
// 				tmpMap[target-value] = value
// 			}
// 		}
// 	}

// 	return res
// //  312/312 cases passed (1574 ms)
// //  Your runtime beats 10.74 % of golang submissions
// //  Your memory usage beats 5.41 % of golang submissions (10.5 MB)
// }
