package questions

/*
 * @lc app=leetcode id=19 lang=golang
 *
 * [19] Remove Nth Node From End of List
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
// 第一個迴圈計算長度
// 第二個迴圈尋找要提除的Node
func removeNthFromEnd(head *ListNode, n int) *ListNode {
	curr := head
	var len int

	for curr != nil {
		len++
		curr = curr.Next
	}

	curr = head
	target := len - n
	if target == 0 {
		return head.Next
	}
	for ix := 1; ix <= target; ix++ {
		if ix != target {
			curr = curr.Next
			continue
		}

		curr.Next = curr.Next.Next
		break
	}

	return head
}

// @lc code=end
