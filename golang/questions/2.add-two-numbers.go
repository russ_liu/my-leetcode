package questions

type ListNode struct {
	Val  int
	Next *ListNode
}

/*
 * @lc app=leetcode id=2 lang=golang
 *
 * [2] Add Two Numbers
 */

// @lc code=start
// Definition for singly-linked list.

// 需要注意進位的判斷；每一次都要檢查相加有無進位；
// 指標也要位移到下個位數才能在相加。
func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	curr := &ListNode{
		Val:  0,
		Next: nil,
	}
	rtn := curr
	nL1 := l1
	nL2 := l2

	for curr != nil || nL1 != nil || nL2 != nil {
		carry := 0
		if nL1 != nil {
			curr.Val += nL1.Val
			nL1 = nL1.Next
		} else {
			nL1 = nil
		}

		if nL2 != nil {
			curr.Val += nL2.Val
			nL2 = nL2.Next
		} else {
			nL2 = nil
		}

		if curr.Val >= 10 {
			curr.Val %= 10
			carry = 1
		}

		if carry > 0 || nL1 != nil || nL2 != nil {
			curr.Next = &ListNode{
				Val:  carry,
				Next: nil,
			}
		}
		curr = curr.Next
	}

	return rtn
}

// @lc code=end
