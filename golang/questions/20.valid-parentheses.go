package questions

/*
 * @lc app=leetcode id=20 lang=golang
 *
 * [20] Valid Parentheses
 */

// @lc code=start
// 括弧號的開始與結束邏輯跟 stack一樣
func isValid(s string) bool {
	stack := []rune{}
	table := map[rune]rune{
		'{': '}',
		'(': ')',
		'[': ']',
	}
	for _, char := range s {
		switch char {
		case '(', '[', '{':
			stack = append(stack, table[char])
		case ')', ']', '}':
			if len(stack) == 0 || stack[len(stack)-1] != char {
				return false
			}
			stack = stack[:len(stack)-1]
		}
	}

	return len(stack) == 0
}

// @lc code=end
