package questions

/*
 * @lc app=leetcode id=5 lang=golang
 *
 * [5] Longest Palindromic Substring
 */

// @lc code=start
func longestPalindrome(s string) string {
	var start, end int
	for idx := range s {
		len := maxPalindromeLen(s, idx, idx)
		len2 := maxPalindromeLen(s, idx, idx+1)
		curLen := maxForQuestion5(len, len2)
		if curLen > end-start {
			start = idx - (curLen-1)/2
			end = idx + curLen/2
		}
	}

	return s[start : end+1]
}

func maxForQuestion5(x, y int) int {
	if x > y {
		return x
	} else if y > x {
		return y
	} else {
		return x
	}
}

func maxPalindromeLen(s string, L, R int) int {
	for L >= 0 && R < len(s) && s[L] == s[R] {
		L--
		R++
	}

	return R - L - 1
}

// @lc code=end
// var PublicTest = longestPalindrome
