package questions

/*
 * @lc app=leetcode id=1 lang=golang
 *
 * [1] Two Sum
 */

// @lc code=start
// 利用Map[ target- nums[?] ] = index 這個結構，存下對應的index，
// 這樣在迴圈裡搜尋符合的數字就是答案了。
func twoSum(nums []int, target int) []int {
	rtnMap := make(map[int]int)
	for ix, value := range nums {
		if _, ok := rtnMap[value]; ok {
			return []int{rtnMap[value], ix}
		} else {
			rtnMap[target-value] = ix
		}
	}

	return []int{}
}

// @lc code=end
