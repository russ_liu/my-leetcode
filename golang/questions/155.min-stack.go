package questions

/*
 * @lc app=leetcode id=155 lang=golang
 *
 * [155] Min Stack
 */

// @lc code=start
type MinStack struct {
	array []int
}

func Constructor() MinStack {
	return MinStack{
		array: make([]int, 0),
	}
}

func (ms *MinStack) Push(val int) {
	ms.array = append(ms.array, val)
}

func (ms *MinStack) Pop() {
	ms.array = ms.array[:len(ms.array)-1]
}

func (ms *MinStack) Top() int {
	return ms.array[len(ms.array)-1]
}

func (ms *MinStack) GetMin() int {
	var rtn int
	for idx, e := range ms.array {
		if idx == 0 || e < rtn {
			rtn = e
		}
	}

	return rtn
}

/**
 * Your MinStack object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Push(val);
 * obj.Pop();
 * param_3 := obj.Top();
 * param_4 := obj.GetMin();
 */
// @lc code=end
