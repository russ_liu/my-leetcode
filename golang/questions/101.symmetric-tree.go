package questions

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

/*
 * @lc app=leetcode id=101 lang=golang
 *
 * [101] Symmetric Tree
 */

// @lc code=start
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

func isSymmetric(root *TreeNode) bool {
	return isSymmetricRecursive(root.Left, root.Right)
}

func isSymmetricRecursive(a *TreeNode, b *TreeNode) bool {

	if a == nil && b == nil {
		return true
	}

	if a == nil || b == nil {
		return false
	}

	if a.Val != b.Val {
		return false
	}
	return isSymmetricRecursive(a.Left, b.Right) &&
		isSymmetricRecursive(a.Right, b.Left)
}

// @lc code=end
